"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
var Schema = mongoose.Schema;
const LiteratureSchema = new Schema({
    _id: { type: Schema.Types.ObjectId },
    author: String,
    title: String,
    offset: Number,
});
exports.UserSchema = new Schema({
    first_name: String,
    last_name: String,
    email: String,
    type: String,
    phone: String,
    address: String,
    description: String,
    literature: [LiteratureSchema],
    created_at: { type: Date, default: Date.now }
});
//# sourceMappingURL=user.schema.js.map