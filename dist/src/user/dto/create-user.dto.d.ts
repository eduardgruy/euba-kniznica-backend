export declare class CreateUserDTO {
    readonly first_name: string;
    readonly last_name: string;
    readonly email: string;
    readonly type: string;
    readonly phone: string;
    readonly address: string;
    readonly description: string;
    readonly Literature: {
        readonly bookID: string;
        readonly author: string;
        readonly title: string;
        readonly offset: number;
    };
    readonly created_at: Date;
}
