export declare class CreateUserLiteratureDTO {
    readonly _id: string;
    readonly author: string;
    readonly title: string;
    readonly offset: number;
}
