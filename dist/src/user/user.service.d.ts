import { Model } from 'mongoose';
import { User } from './interfaces/user.interface';
import { CreateUserDTO } from './dto/create-user.dto';
import { CreateUserLiteratureDTO } from './dto/create-user-literature.dto';
export declare class UserService {
    private readonly userModel;
    constructor(userModel: Model<User>);
    getAllUser(): Promise<User[]>;
    getUser(userID: any): Promise<User>;
    addUser(createUserDTO: CreateUserDTO): Promise<User>;
    updateUser(userID: any, createUserDTO: CreateUserDTO): Promise<User>;
    deleteUser(userID: any): Promise<any>;
    addLiterature(userID: any, createUserLiteratureDTO: CreateUserLiteratureDTO): Promise<any>;
    getLiterature(userID: any): Promise<any>;
    updateLiterature(userID: any, createUserLiteratureDTO: CreateUserLiteratureDTO): Promise<any>;
}
