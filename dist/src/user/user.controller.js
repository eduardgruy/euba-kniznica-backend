"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const user_service_1 = require("./user.service");
const create_user_dto_1 = require("./dto/create-user.dto");
const create_user_literature_dto_1 = require("./dto/create-user-literature.dto");
let UserController = class UserController {
    constructor(userService) {
        this.userService = userService;
    }
    addUser(res, createUserDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.userService.addUser(createUserDTO);
            return res.status(common_1.HttpStatus.OK).json({
                message: 'User has been created successfully',
                user,
            });
        });
    }
    getAllUser(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield this.userService.getAllUser();
            return res.status(common_1.HttpStatus.OK).json(users);
        });
    }
    getUser(res, userID) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.userService.getUser(userID);
            if (!user)
                throw new common_1.NotFoundException('User does not exist!');
            return res.status(common_1.HttpStatus.OK).json(user);
        });
    }
    updateUser(res, userID, createUserDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.userService.updateUser(userID, createUserDTO);
            if (!user)
                throw new common_1.NotFoundException('User does not exist!');
            return res.status(common_1.HttpStatus.OK).json({
                message: 'User has been successfully updated',
                user,
            });
        });
    }
    deleteUser(res, userID) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.userService.deleteUser(userID);
            if (!user)
                throw new common_1.NotFoundException('User does not exist');
            return res.status(common_1.HttpStatus.OK).json({
                message: 'User has been deleted',
                user,
            });
        });
    }
    getUserLiterature(res, userID) {
        return __awaiter(this, void 0, void 0, function* () {
            const literature = yield this.userService.getLiterature(userID);
            return res.status(common_1.HttpStatus.OK).json(literature);
        });
    }
    addUserLiterature(res, userID, createUserLiteratureDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const literature = yield this.userService.addLiterature(userID, createUserLiteratureDTO);
            return res.status(common_1.HttpStatus.OK).json({
                message: 'Literate has been added successfully',
                literature,
            });
        });
    }
    updateUserLiterature(res, userID, createUserLiteratureDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('this is DTO from controller:', createUserLiteratureDTO);
            const user = yield this.userService.updateLiterature(userID, createUserLiteratureDTO);
            if (!user)
                throw new common_1.NotFoundException('User does not exist!');
            return res.status(common_1.HttpStatus.OK).json({
                message: 'User has been successfully updated',
                user,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, create_user_dto_1.CreateUserDTO]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "addUser", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getAllUser", null);
__decorate([
    common_1.Get(':userID'),
    __param(0, common_1.Res()), __param(1, common_1.Param('userID')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getUser", null);
__decorate([
    common_1.Put(':userID'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('userID')),
    __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, create_user_dto_1.CreateUserDTO]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "updateUser", null);
__decorate([
    common_1.Delete(),
    __param(0, common_1.Res()), __param(1, common_1.Query('userID')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "deleteUser", null);
__decorate([
    common_1.Get(':userID/literature'),
    __param(0, common_1.Res()), __param(1, common_1.Param('userID')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getUserLiterature", null);
__decorate([
    common_1.Post(':userID/literature'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('userID')),
    __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, create_user_literature_dto_1.CreateUserLiteratureDTO]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "addUserLiterature", null);
__decorate([
    common_1.Put(':userID/literature'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('userID')),
    __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, create_user_literature_dto_1.CreateUserLiteratureDTO]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "updateUserLiterature", null);
UserController = __decorate([
    common_1.Controller('users'),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map