import { UserService } from './user.service';
import { CreateUserDTO } from './dto/create-user.dto';
import { CreateUserLiteratureDTO } from './dto/create-user-literature.dto';
export declare class UserController {
    private userService;
    constructor(userService: UserService);
    addUser(res: any, createUserDTO: CreateUserDTO): Promise<any>;
    getAllUser(res: any): Promise<any>;
    getUser(res: any, userID: any): Promise<any>;
    updateUser(res: any, userID: any, createUserDTO: CreateUserDTO): Promise<any>;
    deleteUser(res: any, userID: any): Promise<any>;
    getUserLiterature(res: any, userID: any): Promise<any>;
    addUserLiterature(res: any, userID: any, createUserLiteratureDTO: CreateUserLiteratureDTO): Promise<any>;
    updateUserLiterature(res: any, userID: any, createUserLiteratureDTO: CreateUserLiteratureDTO): Promise<any>;
}
