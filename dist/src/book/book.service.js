"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
const fs = require('fs');
let BookService = class BookService {
    constructor(bookModel) {
        this.bookModel = bookModel;
    }
    getAllBook() {
        return __awaiter(this, void 0, void 0, function* () {
            const books = yield this.bookModel.find().exec();
            return books;
        });
    }
    getBook(bookID) {
        return __awaiter(this, void 0, void 0, function* () {
            const book = yield this.bookModel.findById(bookID).exec();
            return book;
        });
    }
    addBook(createBookDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const newBook = yield this.bookModel(createBookDTO);
            return newBook.save();
        });
    }
    updateBook(bookID, createBookDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const updatedBook = yield this.bookModel.findByIdAndUpdate(bookID, createBookDTO, { new: true });
            return updatedBook;
        });
    }
    deleteBook(bookID) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedBook = yield this.bookModel.findByIdAndRemove(bookID).exec();
            return deletedBook;
        });
    }
    readBook(bookID, offs) {
        return __awaiter(this, void 0, void 0, function* () {
            const book = yield this.bookModel.findById(bookID).exec();
            const stream = fs.createReadStream(__dirname + book.location, {
                encoding: 'utf8',
                start: Number(offs),
                end: Number(offs) + 1000,
            });
            const data = yield stream[Symbol.asyncIterator]().next();
            const lastDot = data.value.lastIndexOf('.') + 1;
            return {
                value: data.value.slice(0, lastDot),
                offset: Number(offs) + Buffer.byteLength(data.value.slice(0, lastDot), 'utf8'),
            };
            ;
        });
    }
};
BookService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('Book')),
    __metadata("design:paramtypes", [typeof (_a = typeof mongoose_1.Model !== "undefined" && mongoose_1.Model) === "function" ? _a : Object])
], BookService);
exports.BookService = BookService;
//# sourceMappingURL=book.service.js.map