"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.BookSchema = new mongoose.Schema({
    id: Number,
    author: String,
    title: String,
    category: String,
    year: String,
    description: String,
    type: String,
    location: String,
    created_at: { type: Date, default: Date.now }
});
//# sourceMappingURL=book.schema.js.map