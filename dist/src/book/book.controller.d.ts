import { BookService } from './book.service';
import { CreateBookDTO } from './dto/create-book.dto';
export declare class BookController {
    private bookService;
    constructor(bookService: BookService);
    readBook(res: any, bookID: any, offset: any): Promise<any>;
    addBook(res: any, createBookDTO: CreateBookDTO): Promise<any>;
    getAllBook(res: any): Promise<any>;
    getBook(res: any, bookID: any): Promise<any>;
    updateBook(res: any, bookID: any, createBookDTO: CreateBookDTO): Promise<any>;
    deleteBook(res: any, bookID: any): Promise<any>;
}
