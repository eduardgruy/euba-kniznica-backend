import { Model } from 'mongoose';
import { Book } from './interfaces/book.interface';
import { CreateBookDTO } from './dto/create-book.dto';
export declare class BookService {
    private readonly bookModel;
    constructor(bookModel: Model<Book>);
    getAllBook(): Promise<Book[]>;
    getBook(bookID: any): Promise<Book>;
    addBook(createBookDTO: CreateBookDTO): Promise<Book>;
    updateBook(bookID: any, createBookDTO: CreateBookDTO): Promise<Book>;
    deleteBook(bookID: any): Promise<any>;
    readBook(bookID: any, offs: any): Promise<any>;
}
