export declare class CreateBookDTO {
    readonly author: string;
    readonly title: string;
    readonly category: string;
    readonly year: string;
    readonly description: string;
    readonly type: string;
    readonly location: string;
    readonly created_at: Date;
}
