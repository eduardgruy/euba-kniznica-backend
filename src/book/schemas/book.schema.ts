import * as mongoose from 'mongoose';
const Schema = mongoose.Schema;
// schéma kníh
export const BookSchema = new Schema({
    author: String,
    title: String,
    category: String,
    year: Number,
    description: String,
    type: String,
    location: String, 
    created_at: { type: Date, default: Date.now }
})