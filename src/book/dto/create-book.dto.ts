export class CreateBookDTO {

    // readonly id: number;
    readonly author: string;
    readonly title: string;
    readonly category: string;
    readonly year: string;
    readonly description: string;
    readonly type: string;
     location: string; 
    readonly created_at: Date;
    readonly payload: any;

}