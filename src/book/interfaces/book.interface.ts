// ./src/book/interfaces/book.interface.ts
import { Document } from 'mongoose';

export interface Book extends Document {
    readonly id: number;
    readonly author: string;
    readonly title: string;
    readonly category: string;
    readonly year: string;
    readonly description: string;
    readonly type: string;
    readonly location: string; 
    readonly created_at: Date;
}