import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Book } from './interfaces/book.interface';
import { CreateBookDTO } from './dto/create-book.dto';
const fs = require('fs');

@Injectable()
export class BookService {
  constructor(@InjectModel('Book') private readonly bookModel: Model<Book>) {}
  // Vyber všetky knihy
  async getAllBook(): Promise<Book[]> {
    const books = await this.bookModel.find().exec();
    return books;
  }
  // Vyber knihu podľa ID
  async getBook(bookID): Promise<Book> {
    const book = await this.bookModel.findById(bookID).exec();
    return book;
  }
  // Pridaj knihu
  async addBook(createBookDTO: CreateBookDTO): Promise<Book> {
    const fs = require('fs');
    const buf = new Buffer(createBookDTO.payload, 'base64');
    const location = '/tmp/books/' + createBookDTO.author.replace(/\s/g, '').toLowerCase() + '-' + createBookDTO.title.replace(/\s/g, '').toLowerCase()
    createBookDTO.location = location
    // zapis knihu na filesystem
    fs.writeFile(location , buf, async function(err) {
        if(err) {
            return console.log(err);
        }

    }); 
    // uloz knihu do databazy
    const newBook = await this.bookModel(createBookDTO);
    return newBook.save(); 
  }
  // Aktualizuj informácie o knihe
  async updateBook(bookID, createBookDTO: CreateBookDTO): Promise<Book> {
    const updatedBook = await this.bookModel.findByIdAndUpdate(
      bookID,
      createBookDTO,
      { new: true },
    );
    return updatedBook;
  }
  // Zmaž knihu podľa ID
  async deleteBook(bookID): Promise<any> {
    const deletedBook = await this.bookModel.findByIdAndRemove(bookID).exec();
    return deletedBook;
  }

  //čítaj knihy podľa ID
  async readBook(bookID, offs): Promise<any> {
    //nájdi knihu
    const book = await this.bookModel.findById(bookID).exec();
    //vytvor dátový tok zo súbory knihy od pozície po pozíciu + 1000
    let lastDot = new Number;
    let offset = Number(offs);
    let data;
    let length = 1000;
    do {
      const stream = fs.createReadStream(book.location, {
        encoding: 'utf8',
        start: offset,
        end: offset + length,
      });
      // prečítaj dátový tok
      data = await stream[Symbol.asyncIterator]().next();
      // Nájdi poslednú botku vo texte
      lastDot = data.value.lastIndexOf('.') + 1;
      length += 1000;
      //ak je lastDot 0 tak v useku nenachadzala ukoncena veta
      //zoberieme teda vacsi usek
    } while (lastDot == 0);
    // Vráť dáta po poslednú botku a pozíciu poslednej botky
    return {
      value: data.value.slice(0, lastDot),
      offset:
        Number(offs) + Buffer.byteLength(data.value.slice(0, lastDot), 'utf8'),
    };
  }
}
