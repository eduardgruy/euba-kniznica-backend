import {
  Controller,
  Get,
  Res,
  HttpStatus,
  Post,
  Body,
  Put,
  Query,
  NotFoundException,
  Delete,
  Param,
} from '@nestjs/common';
import { BookService } from './book.service';
import { CreateBookDTO } from './dto/create-book.dto';

@Controller('books')
export class BookController {
  constructor(private bookService: BookService) {}

  // čítaj knihu
  @Get(':bookID/read/:offset')
  async readBook(@Res() res, @Param('bookID') bookID, @Param('offset') offset) {
    const response = await this.bookService.readBook(bookID, offset);
    return res.status(HttpStatus.OK).json({
      response,
    });
  }

  // Pridaj knihu
  @Post()
  async addBook(@Res() res, @Body() createBookDTO: CreateBookDTO) {
    const book = await this.bookService.addBook(createBookDTO);
    return res.status(HttpStatus.OK).json({
      message: 'Book has been created successfully',
      book,
    });
  }

  // Controller - Vyber všetky knihy
  @Get()
  async getAllBook(@Res() res) {
    const books = await this.bookService.getAllBook();
    return res.status(HttpStatus.OK).json(books);
  }

  // Vyber knihu podľa ID
  @Get(':bookID')
  async getBook(@Res() res, @Param('bookID') bookID) {
    const book = await this.bookService.getBook(bookID);
    if (!book) throw new NotFoundException('Book does not exist!');
    return res.status(HttpStatus.OK).json(book);
  }
  // Uprav knihu
  @Put(':bookID')
  async updateBook(
    @Res() res,
    @Param('bookID') bookID,
    @Body() createBookDTO: CreateBookDTO,
  ) {
    const book = await this.bookService.updateBook(bookID, createBookDTO);
    if (!book) throw new NotFoundException('Book does not exist!');
    return res.status(HttpStatus.OK).json({
      message: 'Book has been successfully updated',
      book,
    });
  }

  // Zmaž knihu
  @Delete(':bookID')
  async deleteBook(@Res() res, @Param('bookID') bookID) {
    const book = await this.bookService.deleteBook(bookID);
    if (!book) throw new NotFoundException('Book does not exist');
    return res.status(HttpStatus.OK).json({
      message: 'Book has been deleted',
      book,
    });
  }
}
