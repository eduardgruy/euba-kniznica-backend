// ./src/user/user.service.ts
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './interfaces/user.interface';
import { CreateUserDTO } from './dto/create-user.dto';
import { CreateUserLiteratureDTO } from './dto/create-user-literature.dto';

@Injectable()
export class UserService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}
  // fetch all users
  async getAllUser(): Promise<User[]> {
    const users = await this.userModel.find().exec();
    return users;
  }
  // Get a single user
  async getUser(userID): Promise<User> {
    const user = await this.userModel.findById(userID).exec();
    return user;
  }
  // autentifikácia užívateľa
  async authUser(username, password): Promise<Boolean> {
    const user = await this.userModel.findById(username).exec();
    if (user.password == password) return true;
    else return false;
  }
  // post a single user
  async addUser(createUserDTO: CreateUserDTO): Promise<User> {
    console.log(createUserDTO)
    const newUser = await this.userModel(createUserDTO);
    return newUser.save();
  }
  // Edit user details
  async updateUser(userID, createUserDTO: CreateUserDTO): Promise<User> {
    const updatedUser = await this.userModel.findByIdAndUpdate(
      userID,
      createUserDTO,
      { new: true },
    );
    return updatedUser;
  }
  // Delete a user
  async deleteUser(userID): Promise<any> {
    const deletedUser = await this.userModel.findByIdAndRemove(userID);
    return deletedUser;
  }

  //add user's literature
  async addLiterature(
    userID,
    createUserLiteratureDTO: CreateUserLiteratureDTO,
  ): Promise<any> {
    const user = await this.userModel.findById(userID).exec();
    console.log(user);
    console.log(createUserLiteratureDTO);
    user.literature.push(createUserLiteratureDTO);
    user.save(function(err) {
      console.log('Success!', err);
    });
    return user;
  }
  // Vráť rozčítané knihy užívateľa
  async getLiterature(userID): Promise<any> {
    const user = await this.userModel.findById(userID).exec();
    return user.literature;
  }
  //update user's literature
  async updateLiterature(
    userID,
    createUserLiteratureDTO: CreateUserLiteratureDTO,
  ): Promise<any> {
    const user = await this.userModel.findById(userID).exec();
    const literature = user.literature.id(createUserLiteratureDTO._id);

    if (literature == null) user.literature.push(createUserLiteratureDTO);
    else literature.offset = createUserLiteratureDTO.offset;
    user.save(function(err) {
      console.log('Success!', err);
    });
    return user;
  }
}
