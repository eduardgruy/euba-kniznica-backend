import * as mongoose from 'mongoose';
const Schema = mongoose.Schema;

// schéma rozčítaných kníh
const LiteratureSchema = new Schema({
    _id: {type: Schema.Types.ObjectId},
    author: String,
    title: String,
    offset: Number,
})

// schéma užívateľa 
export const UserSchema = new Schema({
    _id: String,
    first_name: String,
    last_name: String,
    password: String,
    type: String,
    phone: String,
    address: String,
    description: String,
    literature: [LiteratureSchema],
    created_at: { type: Date, default: Date.now }
})