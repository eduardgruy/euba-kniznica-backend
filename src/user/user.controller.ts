import {
  Controller,
  Get,
  Res,
  HttpStatus,
  Post,
  Body,
  Put,
  Query,
  NotFoundException,
  UnauthorizedException,
  Delete,
  Param,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDTO, AuthUserDTO } from './dto/create-user.dto';
import { CreateUserLiteratureDTO } from './dto/create-user-literature.dto';

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  // add a user
  @Post()
  async addUser(@Res() res, @Body() createUserDTO: CreateUserDTO) {
    console.log(createUserDTO)
    const user = await this.userService.addUser(createUserDTO);
    return res.status(HttpStatus.OK).json({
      message: 'User has been created successfully',
      user,
    });
  }

  // Retrieve users list
  @Get()
  async getAllUser(@Res() res) {
    const users = await this.userService.getAllUser();
    return res.status(HttpStatus.OK).json(users);
  }

  // controller autentifikačný objekr
  @Post('/authenticate')
  async authUser(@Res() res, @Body() authUserDTO: AuthUserDTO) {
    const response = await this.userService.authUser(
      authUserDTO.username,
      authUserDTO.password,
    );
    if (response === false)
      throw new UnauthorizedException('Wrong username or password!');
    return res.status(HttpStatus.OK).json({
      username: authUserDTO.username,
    });
  }
  // Fetch a particular user using ID
  @Get(':userID')
  async getUser(@Res() res, @Param('userID') userID) {
    const user = await this.userService.getUser(userID);
    if (!user) throw new NotFoundException('User does not exist!');
    return res.status(HttpStatus.OK).json(user);
  }

  @Put(':userID')
  async updateUser(
    @Res() res,
    @Param('userID') userID,
    @Body() createUserDTO: CreateUserDTO,
  ) {
    const user = await this.userService.updateUser(userID, createUserDTO);
    if (!user) throw new NotFoundException('User does not exist!');
    return res.status(HttpStatus.OK).json({
      message: 'User has been successfully updated',
      user,
    });
  }

  // Delete a user
  @Delete()
  async deleteUser(@Res() res, @Query('userID') userID) {
    const user = await this.userService.deleteUser(userID);
    if (!user) throw new NotFoundException('User does not exist');
    return res.status(HttpStatus.OK).json({
      message: 'User has been deleted',
      user,
    });
  }

  // Vráť rozčítané knihy užívateľa
  @Get(':userID/literature')
  async getUserLiterature(@Res() res, @Param('userID') userID) {
    const literature = await this.userService.getLiterature(userID);
    return res.status(HttpStatus.OK).json(literature);
  }
  // add user Literature
  @Post(':userID/literature')
  async addUserLiterature(
    @Res() res,
    @Param('userID') userID,
    @Body() createUserLiteratureDTO: CreateUserLiteratureDTO,
  ) {
    const literature = await this.userService.addLiterature(
      userID,
      createUserLiteratureDTO,
    );
    return res.status(HttpStatus.OK).json({
      message: 'Literate has been added successfully',
      literature,
    });
  }
  //update user's literature
  @Put(':userID/literature')
  async updateUserLiterature(
    @Res() res,
    @Param('userID') userID,
    @Body() createUserLiteratureDTO: CreateUserLiteratureDTO,
  ) {
    console.log('this is DTO from controller:', createUserLiteratureDTO);
    console.log('This is user', userID);
    const user = await this.userService.updateLiterature(
      userID,
      createUserLiteratureDTO,
    );
    if (!user) throw new NotFoundException('User does not exist!');
    return res.status(HttpStatus.OK).json({
      message: 'User has been successfully updated',
      user,
    });
  }
}
