// ./src/user/dto/create-user.dto.ts
export class CreateUserLiteratureDTO {
        readonly _id: string;
        readonly author: string;
        readonly title: string;
        readonly offset: number;
}