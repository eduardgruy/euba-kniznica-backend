// ./src/user/dto/create-user.dto.ts
export class CreateUserDTO {
    readonly _id: string;
    readonly first_name: string;
    readonly last_name: string;
    readonly password: string;
    readonly type: string;
    readonly phone: string;
    readonly address: string;
    readonly description: string;
    readonly Literature: {
        readonly bookID: string,
        readonly author: string,
        readonly title: string, 
        readonly offset: number,}
    readonly created_at: Date;
}

export class AuthUserDTO {
    readonly username: string;
    readonly password: string;
}