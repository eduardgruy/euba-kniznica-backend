import { NestFactory } from '@nestjs/core';
import * as bodyParser from 'body-parser';
import { AppModule } from './app.module';
async function bootstrap() {

  const fs = require('fs');
  const httpsOptions = {
    key: fs.readFileSync('/etc/letsencrypt/live/euba-kniznica.northeurope.cloudapp.azure.com/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/euba-kniznica.northeurope.cloudapp.azure.com/fullchain.pem'),
  };

  const app = await NestFactory.create(AppModule, {httpsOptions});
  app.use(bodyParser.json({limit: '50mb'}));
  app.enableCors(); // add this line
  await app.listen(443);
}
bootstrap();